var main = function() {
  $('.dropdown-toggle').click(function() {
    $('.dropdown-menu').toggle();
  });

  
  $('.arrow-next').click(function() {
    var currentSlide = $('.active-slide');
    var nextSlide = currentSlide.next();

    var currentDot = $('.active-dot');
    var nextDot = currentDot.next();

    if(nextSlide.length === 0) {
      nextSlide = $('.slide').first();
      nextDot = $('.dot').first();
    }
    
    currentSlide.fadeOut(600).removeClass('active-slide');
    nextSlide.fadeIn(600).addClass('active-slide');

    currentDot.removeClass('active-dot');
    nextDot.addClass('active-dot');
  });


  $('.arrow-prev').click(function() {
    var currentSlide = $('.active-slide');
    var prevSlide = currentSlide.prev();

    var currentDot = $('.active-dot');
    var prevDot = currentDot.prev();

    if(prevSlide.length === 0) {
      prevSlide = $('.slide').last();
      prevDot = $('.dot').last();
    }
    
    currentSlide.fadeOut(600).removeClass('active-slide');
    prevSlide.fadeIn(600).addClass('active-slide');

    currentDot.removeClass('active-dot');
    prevDot.addClass('active-dot');
  });

};

$(document).ready(main);

var spreadsheetID = "1zNnoVbRzocNVKw0Y5KtQmx6EN-Hevr1SQdsfMdy2oMg";
 // Make sure it is public or set to Anyone with link can view 
 var url = "https://spreadsheets.google.com/feeds/list/" + spreadsheetID + "/1/public/values?alt=json";
 
 $.getJSON(url, function(data) {
 
  var entry = data.feed.entry;
  
  $(entry).each(function(){

    // Column names are name, age, etc.
    $('.results').append('<li class="list-group-item ">'+this.gsx$post.$t+'</li>');
    //$('<li>').text(this.gsx$id.$t).appendTo('.post');
    //$('.results').append('<h2>'+this.gsx$id.$t+'</h2><p>'+this.gsx$firstname.$t+'</p>');
  });
 });



var urld = "https://spreadsheets.google.com/feeds/list/" + spreadsheetID + "/6/public/values?alt=json";
    $.getJSON(urld, function(data) {
 
    var entry = data.feed.entry;
  
    $(entry).each(function(){

    // Column names are name, age, etc.
    $('.resultsd').append('<div class="col-md-4">'+
                '<div class="panel panel-info">'+
                    '<div class="panel-heading">'+
                        '<h5>'+this.gsx$name.$t+'</h5>'+
                        '<a href="'+this.gsx$web.$t+'" target="_blank">'+this.gsx$web.$t+'</a>'+ 
                    '</div>'+
                    '<div class="panel-body">'+
                        '<p>'+this.gsx$introduction.$t+'</p>'+
                    '</div>'+
                '</div>'+
            '</div>');

    //$('<li>').text(this.gsx$id.$t).appendTo('.post');
    //$('.resultsd').append('<h2>'+this.gsx$name.$t+'</h2>');
  });
 });

var urlc = "https://spreadsheets.google.com/feeds/list/" + spreadsheetID + "/3/public/values?alt=json";
    $.getJSON(urlc, function(data) {
 
    var entry = data.feed.entry;
  
    $(entry).each(function(){

    // Column names are name, age, etc.
    $('.contactd').append('<div class="col-md-4">'+
                '<div class="panel panel-success">'+
                    '<div class="panel-heading">'+
                        '<h4><i class="fa fa-fw fa-check"></i>'+this.gsx$name.$t+'<span><small>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+this.gsx$title.$t+'</small></span></h4>'+
                        '<p>'+this.gsx$phone.$t+'<br>'+this.gsx$email.$t+'</p>'+ 
                    '</div>'+
                    '<div class="panel-body">'+
                        '<p>'+this.gsx$responsibilities.$t+'</p>'+   
                    '</div>'+
                '</div>'+
            '</div>');

  });
 });





